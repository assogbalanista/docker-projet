require('dotenv').config();
const Server = require('./models/server');
const server = new Server();

server.listen();

const fs = require('fs');

// Vérification de la présence du dossier node_modules
if (!fs.existsSync('./node_modules')) {
    console.log('Les dépendances npm ne sont pas installées. Installation en cours...');
    const { execSync } = require('child_process');
    execSync('npm install');
}


const path = require('path');

// Vérification de la présence des fichiers statiques
const staticFiles = ['client', 'controllers', 'middleware', 'models', 'routes']; // Ajoutez ici les dossiers contenant vos fichiers statiques
staticFiles.forEach(dir => {
    if (!fs.existsSync(path.join(__dirname, dir))) {
        console.error(`Le dossier ${dir} est manquant.`);
        process.exit(1);
    }
});
