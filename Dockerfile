# Utiliser l'image de base Node.js
FROM node:latest

# Créer le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copier les fichiers de l'application dans le conteneur
COPY ./NodeReact .

# Installer les dépendances
#RUN npm install dotenv

RUN npm install

# Exposer le port sur lequel l'application Node.js s'exécute
EXPOSE 3000

# Commande pour démarrer l'application

CMD ["ls -la", "/"]

CMD ["npm", "start"]


